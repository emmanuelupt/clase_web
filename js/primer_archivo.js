var posicion = 1; // -- Variable para saber el número de casillas que ya dio click
var posiciones = [1,2,3,4,5,6];
var posicionesOcupadas = [];

cargarValores();
setTimeout('ocultarValores()',6000);

function verificacion(e){
    // -- Obtener valor de div
    //console.log(e[0].getAttribute('valor'));
    var valor = e[0].getAttribute('valor');
    if(valor == posicion){
        posicion++;
        e[0].style.backgroundColor="#269b10";
    }else{
        alert("Fallaste")
        e[0].style.backgroundColor="red";
    }
    if((posicion-1) == posiciones.length){
        alert("Que buena memoria tienes");
    }
    // -- Cambiar color de fondo en div
}

function cargarValores (){
    const elemtos = document.querySelectorAll('.casilla');
    elemtos.forEach(elemento =>{
        var numeroAleatorio;
        while ((numeroAleatorio = numAlea(1,6)) && posicionesOcupadas.includes(numeroAleatorio));
        // -- Agregar número a array de posiciones ocupadas para que no se repita
        posicionesOcupadas.push(numeroAleatorio);
        //console.log(numeroAleatorio);
       elemento.setAttribute('valor', numeroAleatorio) ;
       // -- Acceder al h3
       var decendiente = elemento.getElementsByTagName('h3');
       //console.log(decendiente[0]);
        // -- Cambiar el valor del texto (Html) del h3
       decendiente[0].innerHTML = numeroAleatorio;
    });
}

function numAlea (min, max){
     return Math.round(Math.random()*(min-max)+parseInt(max));
}

function ocultarValores (){
    const elemtos = document.querySelectorAll('.casilla');
    elemtos.forEach(elemento =>{
        var decendiente = elemento.getElementsByTagName('h3');
        //console.log(decendiente[0]);
        decendiente[0].innerHTML = "";
    });
}